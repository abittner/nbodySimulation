# Compile program
./compile.sh

# Clean output directory
rm -rf outputs
mkdir outputs

# Execute simulation
./nbodySimulation
