// ========================================================================== //
//                                                                            //
//  A Gravitational N-body Simulation.                                        //
//                                                                            //
//  Author: Adrian Bittner                                                    //
//                                                                            //
// ========================================================================== //

#include <iostream>
#include <iomanip>
#include <fstream>

#include <string>

#include <cstdlib>
#include <cmath>
#include <vector>

#include <Eigen/Dense>

#include <particle.h>
#include <physConstants.h>
#include <config.h>

using namespace std;



// ========================================================================== //
//                             F U N C T I O N S                              //
// ========================================================================== //

void updateAcceleration(vector<nbodyParticle> &particles)
{
    // Calculate the accelerations on the particles by direct summation over all
    // other particles. The calculation takes a numerical softening length
    // (SOFTENING) into account. 
    
    # pragma omp parallel for
    for (int i=0; i<particles.size(); i++) 
    {
        // Set acceleration to zero
        particles[i].acceleration[0] = 0;
        particles[i].acceleration[1] = 0;
        particles[i].acceleration[2] = 0;
        
        // Calculate total acceleration on particle i from all other particles
        for (int j=0; j<particles.size(); j++)
        {
            if (i != j)
            {
                double radius = (particles[j].position - particles[i].position).norm();

                particles[i].acceleration[0] += G * particles[j].mass * (particles[j].position[0]-particles[i].position[0]) / (pow(radius*radius + SOFTENING*SOFTENING, 3/2.));
                particles[i].acceleration[1] += G * particles[j].mass * (particles[j].position[1]-particles[i].position[1]) / (pow(radius*radius + SOFTENING*SOFTENING, 3/2.));
                particles[i].acceleration[2] += G * particles[j].mass * (particles[j].position[2]-particles[i].position[2]) / (pow(radius*radius + SOFTENING*SOFTENING, 3/2.));
            }
        }
    }
}


void updateVelocity(vector<nbodyParticle> &particles, double dt)
{
    // Calculate the velocities of all particles based on the current time step
    // and acceleration.
    //

    # pragma omp parallel for
    for (int i=0; i<particles.size(); i++) 
    {
        particles[i].velocity[0] += particles[i].acceleration[0] * dt;        
        particles[i].velocity[1] += particles[i].acceleration[1] * dt;        
        particles[i].velocity[2] += particles[i].acceleration[2] * dt;        
    }
}


void updatePosition(vector<nbodyParticle> &particles, double dt)
{
    // Calculate the position of all particles based on the current time step
    // and velocity. 
    //

    # pragma omp parallel for
    for (int i=0; i<particles.size(); i++) 
    {
        particles[i].position[0] += particles[i].velocity[0] * dt;
        particles[i].position[1] += particles[i].velocity[1] * dt;
        particles[i].position[2] += particles[i].velocity[2] * dt;
    }
}


void initialiseParticles(vector<nbodyParticle> &particles)
{
    // Read particle positions, velocities, and masses from file and create an
    // instance of the nbodyParticle class for each particle in the simulation.
    //

    // Input parameters
    double inputPosition_X, inputPosition_Y, inputPosition_Z;
    double inputVelocity_X, inputVelocity_Y, inputVelocity_Z;
    double inputMass;
 
    // Open file and read from it
    ifstream inputFile(inputFileName, ios::in);
    if (inputFile.is_open())
    {
        while (inputFile.good())
        {
            // Read position, velocity, and mass from input file
            inputFile >> inputPosition_X >> inputPosition_Y >> inputPosition_Z >>
                         inputVelocity_X >> inputVelocity_Y >> inputVelocity_Z >> 
                         inputMass;
            if (!inputFile) break;

            // Create an instance of nbodyParticle and set its position, velocity, and mass
            nbodyParticle generateParticle;
            generateParticle.position << inputPosition_X, inputPosition_Y, inputPosition_Z;
            generateParticle.velocity << inputVelocity_X, inputVelocity_Y, inputVelocity_Z;
            generateParticle.mass = inputMass;

            // Add the instance of nbodyParticle to the data vector
            particles.push_back(generateParticle);
        }
    }
    inputFile.close();
    cout << "Generated " << particles.size() << " particles" << endl;
}


void printProgress (int i, double nSteps)
{
    // Print the progress of the simulation in the console. 
    //

    int printFrac = round(nSteps/10000);
    if ( i%printFrac == 0 ) 
    {
        printf("\rRunning simulation: %3.2f%%", i/nSteps*100);
        fflush(stdout);
    }
}


void determineTimeStep (vector<nbodyParticle> &particles, double &dt)
{
    // Determine the time step of the simulation. To this end, determine the
    // maximum velocity currently assigned to an particle, and choose a time
    // step dt that is sufficiently small such that the maximum spatial step
    // size xMax is maintained. 
    //

    double maxVelocity = numeric_limits<double>::min();
    double curVelocity;

    // Determine the maximum absolute velocity of all particles
    for (int j=0; j<particles.size(); j++)
    {
        curVelocity = particles[j].velocity.norm();
        if (maxVelocity < curVelocity)
        {
            maxVelocity = curVelocity;
        }
    }

    // Determine the time step based on the chosen maximum spatial step size. 
    dt = xMax / maxVelocity;
}


void saveSnapshot (vector<nbodyParticle> &allParticles, double time, int snapshotIndex)
{
    // Save the current particle positions. The current time is given in the
    // first column, while all subsequent columns state the spatial coordinates
    // of all particles. 
    //

    double out;
    char filename[100];
    sprintf(filename, "outputs/%06d.dat", snapshotIndex);

    ofstream myfile;
    myfile.open(filename, ios::app);
    myfile << left << setw(15) << time;
    for (int j=0; j<allParticles.size(); j++) 
    {
            out = allParticles[j].position[0]; myfile << left << setw(15) << out;
            out = allParticles[j].position[1]; myfile << left << setw(15) << out;
            out = allParticles[j].position[2]; myfile << left << setw(15) << out;
    }
    myfile << endl;
    myfile.close();
}





// ========================================================================== //
//                         M A I N   F U N C T I O N                          //
// ========================================================================== //
int main(int argc, char** argv) 
{
    cout << endl;


    // Time parameters
    double time = 0;
    int nSteps = (tMax-time)/dt;

    // Data vector, containing the particle class for each particle
    vector<nbodyParticle> particles;

    // Indexing of snapshots
    int snapshotIndex = 0;


    // Create particles from the positions, velocities, and masses defined in the input file.
    initialiseParticles(particles);


    // Initial calculation of acceleration
    updateAcceleration(particles);

    // Time iteration
    for (int i=0; i<=nSteps; i++)
    {
        // Print progress
        printProgress(i, nSteps);


        // Leap-Frog Integration Schemes
        if (INTEGRATION_SCHEME == "KDK")
        {
            // Kick-Drive-Kick (KDK) scheme
            updateVelocity(particles, 0.5*dt);
            updatePosition(particles, 1.0*dt);
            updateAcceleration(particles);
            updateVelocity(particles, 0.5*dt);
        }    
        else if (INTEGRATION_SCHEME == "DKD")
        {
            // Drive-Kick-Drive (DKD) scheme
            updatePosition(particles, 0.5*dt);
            updateAcceleration(particles);
            updateVelocity(particles, 1.0*dt);
            updatePosition(particles, 0.5*dt);
        }
        // TODO: Add other integration schemes: 
        //  * Euler 
        //  * Runge-Kutta
        //  * ....

        
        // Variable time step
        if (VARIABLE_TIME_STEP == true)
        {
            determineTimeStep(particles, dt);
        }


        // Save snapshots
        if (i%SNAPSHOT_FREQUENCY == 0)
        {
            saveSnapshot(particles, time, snapshotIndex); 
            snapshotIndex += 1;
        }
   

        // Update time
        time = time + dt;
    }


    // End of simulation
    cout << endl << "Simulation finished. Exit." << endl;    
    return(0);
}
// ========================================================================== //
// ========================================================================== //
