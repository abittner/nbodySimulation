A Direct-sum Gravitational N-body Simulation
============================================
This package contains a simple, C++ implementation of a gravitational N-body
simulation. The code employs the direct-sum approach for calculating the
accelerations between particles, and offers an option to choose between the
Kick-Drive-Kick (KDK) and Drive-Kick-Drive (DKD) Leap-Frog integration schemes.
It further allows variable time steps, by defining a maximum spatial steps size.


Parallelisation
---------------
The computationally expensive parts of the code are parallelised with OpenMP. To
make use of this parallelisation, use the compiler flag `-fopenmp` and set the
number of parallel threads with `OMP_NUM_THREAD` before running the program. 

An example for the compilation of the package on macOS is provided in
`compile.sh`. 


Configuration
-------------
Basic configurations of the simulation are defined in `config.h`. This file
allows to define, for instance, the time step of the simulation, whether or not
a variable step size is used, the integration scheme, and gravitational
softening length. All available parameters are described in the file itself. 

Units and physical constants, i.e. the gravitational constant, are defined in
`physConstants.h`. By default, the code uses units of solar masses, astronomical
units, and km/s. We note that this file can be used to change the employed
units. 


Input and Output
----------------
The file defining the initial conditions of the particles in the simulation is
`input.dat`. This file defines one N-body particle per row, with the 7 columns
defining the positions (x, y, z), velocities (x, y, z), and mass for each
particle.

The output snapshots are saved in the directory `output/`. The constant
`SNAPSHOT_FREQUENCEY` in `config.h` defines the number of time steps after which
a snapshot is written. A typical snapshot file contains only one line where the
first column states the time of the snapshot. All subsequent columns state the
positions (x, y, z) of the particles, with one particle after another. 


Output Visualisations
---------------------
Examples of gravitational interactions simulated with this code are displayed
below. These visualisations have been generated with the Python routine
`animatePositions.py` included in this package. 

<img src="examples/2body_0.gif" width="275"/> <img src="examples/2body_1.gif" width="275"/> <img src="examples/3body_0.gif" width="275"/> 

<img src="examples/3body_1.gif" width="275"/> <img src="examples/3body_2.gif" width="275"/> <img src="examples/4body_0.gif" width="275"/> 
