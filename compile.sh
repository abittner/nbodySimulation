# Compile program
/usr/local/opt/llvm/bin/clang++ \
    $(pkg-config --libs --cflags /usr/local/Cellar/eigen/3.3.7/share/pkgconfig/eigen3.pc) \
    -mlinker-version=450 -L/usr/local/opt/llvm/lib -fopenmp \
    -I . \
    nbodySimulation.cpp -o nbodySimulation
