// ========================================================================== //
//                                                                            //
//  This file defines the gravitational constant used by the N-body           //
//  simulation code. The constants below can be used to change the units      //
//  employed by the simulation. By default, the simulation uses the units     //
//  solar mass (Msol), astronomical unit (AU), and km/s.                      //
//                                                                            //
//  Inputs and outputs of the simulation are given in these units.            //
//                                                                            //
//  Author: Adrian Bittner                                                    //
//                                                                            //
// ========================================================================== //


// Physical constants
const double G_cgsUnits = 6.672e-8;  // Gravitational constant in CGS Units: cm^3 / g / s^2

const double massUnit = 1.989e33;    // Msol in gramm
const double lengthUnit = 1.496e13;  // AU in cm
const double velocityUnit = 1e5;     // km/s in cm/s
const double timeUnit = lengthUnit/velocityUnit;

const double G = G_cgsUnits * massUnit * timeUnit*timeUnit / lengthUnit/lengthUnit/lengthUnit;
