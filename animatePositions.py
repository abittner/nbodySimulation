import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import glob
import sys


"""
Simple routine to create animations of the particle positions, as produced by 
the N-body simulation code. 
"""


def init():
    fig.clear()


def animate(i): 
    plt.clf()
    plt.xlim([-1*spatialExtent,spatialExtent])
    plt.ylim([-1*spatialExtent,spatialExtent])
    plt.axes().set_aspect('equal')
#    plt.axes().get_xaxis().set_visible(False)    # Turn off x-axis
#    plt.axes().get_yaxis().set_visible(False)    # Turn off y-axis
    
    # Plot current positions and paths of particles
    for o in range(nparticles):
        plt.scatter(positions[i,o*3],  positions[i,o*3+1],  marker='.')
        plt.plot(   positions[:i,o*3], positions[:i,o*3+1], alpha=0.3)


# Useful variables
timestep = 5e-3       # Adapt speed of the animation
spatialExtent = 1.1   # Field-of-view of the animation


# Main plotting procedure
fig = plt.figure()

inputFiles = glob.glob('outputs/*.dat')
inputFiles.sort()
nSnapshots = len(inputFiles)

t = np.zeros(nSnapshots, dtype=int)
nparticles = int((np.genfromtxt(inputFiles[0]).size-1)/3)
positions  = np.zeros((nSnapshots,nparticles*3))

# Read data from snapshots
for i, file in enumerate(inputFiles):
    if i == nSnapshots: break

    data = np.genfromtxt(file)
    t[i] = np.round( data[0] / timestep )
    positions[i,:] = np.array(data[1:])

# Calculate time each snapshot is displayed
for i in range(nSnapshots-1):
    t[i] = t[i+1] - t[i]

# Create a list stating how long each snapshot is displayed
frame_t = []
for i, item in enumerate(t):
    frame_t.extend([i] * item)

# Create animation
ani = anim.FuncAnimation(fig, animate, init_func=init, frames=frame_t, interval=1, repeat=True, save_count=sys.maxsize)

# Save and show animation
plt.tight_layout()
#ani.save('animation.mp4', fps=15)
plt.show()
