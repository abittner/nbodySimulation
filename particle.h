// ========================================================================== //
//                                                                            //
//  Class defining position, velocity, acceleration, and mass of each         //
//  particle in the N-body simulation.                                        //
//                                                                            //
//  Author: Adrian Bittner                                                    //
//                                                                            //
// ========================================================================== //

#include <cstdlib>
#include <Eigen/Dense>

using namespace std;



// Define N-body particle class
class nbodyParticle
{
    public:

        // Constructor
        nbodyParticle();

        // Vectors for position, velocity, and acceleration
        Eigen::Vector3d position;
        Eigen::Vector3d velocity;
        Eigen::Vector3d acceleration;
    
        // Variable for mass
        double mass;
};


// Constructor function: Set all initial values to zero
nbodyParticle::nbodyParticle(void)
{
    position     << 0, 0, 0;
    velocity     << 0, 0, 0;
    acceleration << 0, 0, 0;

    mass = 0;
}
