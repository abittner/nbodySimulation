// ========================================================================== //
//
//  Configuration file for the N-body simulation. 
//
// ========================================================================== //

// Time Parameter
double tMax = 25;    // End time of simulation
double dt   = 1e-6;  // If VARIABLE_TIME_STEP = false, this represent the constant time step of the simulation
                     // If VARIABLE_TIME_STEP = true, this represents the initial time step during the first integration

// Variable time steps
const bool VARIABLE_TIME_STEP = true ;  // Use variable time step
const double xMax = 1e-5;               // Maximum spatial step size if variable time step is used

// Integration Scheme
const string INTEGRATION_SCHEME = "KDK";  // Defines the used integration scheme of the Leap-Frog integrator (KDK or DKD)

// Softening 
const double SOFTENING = 5e-3;  // Gravitational softening length

// Input
const string inputFileName = "input.dat";  // File containing the initial conditions

// Output
const int SNAPSHOT_FREQUENCY = 1000;  // Number of time steps after which a snapshot is saved
